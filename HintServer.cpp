#include <iostream>
#include <memory>
#include <boost/asio.hpp>
#include <boost/bind/bind.hpp>
#include <cstdlib>
#include <iomanip>
#include <vlcpp/vlc.hpp>
#include <Magick++.h>
#include "wtok.h"
#include "wlaup.h"
#include "wmain.hpp"
#include <fstream>
#include <codecvt>
#include <locale>
#include <ctime>
#include <iomanip>
#include <xcb/xcb.h>
#include <xcb/xfixes.h>

using namespace std;
using boost::asio::ip::udp;
using namespace Magick;

int tmp_index = 0;

VLC::Instance instance = VLC::Instance(0, nullptr);
VLC::MediaPlayer mp = VLC::MediaPlayer(instance);
boost::asio::io_context io;
boost::asio::steady_timer hint_timer(io);
int hint_timeout = 0; // a non-positive value means no automatic timeout event
const auto window_timer_expiry = boost::asio::chrono::milliseconds(20);
boost::asio::steady_timer window_timer(io);
xcb_connection_t *connection;
xcb_screen_t *screen;
xcb_window_t win;
xcb_atom_t wm_delete_window_atom;
xcb_atom_t wm_fullscreen_atom;
xcb_atom_t wm_state_atom;

int cmd_showhint(int argc, wchar_t *argv[]);
int cmd_showbg(int argc, wchar_t *argv[]);
int cmd_playmedia(int argc, wchar_t *argv[]);
int cmd_exit(int argc, wchar_t *argv[]);
int cmd_setbg(int argc, wchar_t *argv[]);
int cmd_sethbg(int argc, wchar_t *argv[]);
int cmd_resetbg(int argc, wchar_t *argv[]);
int cmd_resethbg(int argc, wchar_t *argv[]);
int add_caption(const wchar_t *image_source, const wchar_t *image_destination, const wchar_t *text);

wstring s2ws(const string& str)
{
	using convert_typeX = codecvt_utf8<wchar_t>;
	wstring_convert<convert_typeX, wchar_t> converterX;

	return converterX.from_bytes(str);
}

string ws2s(const wstring& wstr)
{
	using convert_typeX = codecvt_utf8<wchar_t>;
	wstring_convert<convert_typeX, wchar_t> converterX;

	return converterX.to_bytes(wstr);
}

struct Command {
	const wchar_t * name;
	int (*functptr) (int argc, wchar_t *argv[]);
};

const Command cmds[] = {
	{L"showhint",	cmd_showhint},
	{L"showbg",		cmd_showbg},
	{L"setbg",		cmd_setbg},
	{L"sethbg",		cmd_sethbg},
	{L"resetbg",	cmd_resetbg},
	{L"resethbg",	cmd_resethbg},
	{L"playmedia",	cmd_playmedia},
	{L"exit",		cmd_exit}
};

const int NCMDS = NELEMS(cmds);

enum {
	NTOKENS	=	16,
	ERROR_NOT_FOUND			=	127,
};

wstring mediadir; // Defaults to ~/Media/
wstring reset_background = L"bg.jpg";
wstring reset_hint_background = L"hbg.jpg";
wstring background = reset_background;
wstring hint_background = reset_hint_background;
wstring log_file = L"";
unsigned port = LAUP_PORT;

int errorCode = ERROR_NONE;

int cmd_setbg(int argc, wchar_t *argv[]) {
	if (argc < 2) {
		wcout << L"Usage: setbg BACKGROUNDFILE!" << endl;
		return 1;
	}
	background = argv[1];
	wcout << L"Background set to " << quoted(background) << "." << endl;
	return 0;
}

int cmd_sethbg(int argc, wchar_t *argv[]) {
	if (argc < 2) {
		wcout << L"Usage: sethbg HINTBACKGROUNDFILE!" << endl;
		return 1;
	}
	hint_background = argv[1];
	wcout << L"Hint background set to " << quoted(hint_background) << "." << endl;
	return 0;
}

int cmd_resetbg(int argc, wchar_t *argv[]) {
	background = reset_background;
	wcout << L"Background reset to " << quoted(background) << "." << endl;
	return 0;
}

int cmd_resethbg(int argc, wchar_t *argv[]) {
	hint_background = reset_hint_background;
	wcout << L"Hint background reset to " << quoted(hint_background) << "." << endl;
	return 0;
}

void w_replace_all(std::wstring &ws, const std::wstring &from, const std::wstring &to) {
	using namespace std;
	size_t pos = 0;
	while ((pos = ws.find(from, pos)) != wstring::npos) {
		ws.replace(pos, from.size(), to);
		pos += to.size();
	}
}

int interpret(wchar_t * line) {
	size_t tokenc;
	bool found = false;
	wchar_t *tokenv[NTOKENS] = {}; // initialize all elements to nullptr
	int tokResult = w_tokenize(line, &tokenc, tokenv, NTOKENS-1);
	// tokenv[argc] will be nullptr
	if (tokResult) {
		if (tokResult == ERROR_TOO_MANY_TOKENS) {
			wprintf(L"Shell: Max number of %d tokens exceeded!\n", NTOKENS);
		}
		if (tokResult == ERROR_INVALID_QUOTE) {
			wprintf(L"Shell: Unclosed quotation mark!\n");
		}
		if (tokResult == ERROR_INVALID_ESCAPE) {
			wprintf(L"Shell: Invalid escape sequence!\n");
		}
		return errorCode = tokResult;
	}
	if (!tokenc) {
		return errorCode = ERROR_NONE;
	}
	for (int i = 0; i < NCMDS; i++) {
		if (!wcscmp(tokenv[0], cmds[i].name)) {
			found = true;
			errorCode = cmds[i].functptr(tokenc, tokenv);
			break;
		}
	}
	if (!found) {
		wprintf(L"Shell: Command '%ls' not found!\n", tokenv[0]);
		errorCode = ERROR_NOT_FOUND;
	}
	return errorCode;
}

class udp_server {
public:
	static const int server_port = 40000;
	static const int buffer_size = 1024;

private:
	udp::socket socket_;
	udp::endpoint remote_endpoint_;
	char recv_buffer_[buffer_size];
	wofstream log_file_;

	void start_receive() {
		socket_.async_receive_from(boost::asio::buffer(recv_buffer_, sizeof(recv_buffer_)), remote_endpoint_,
				boost::bind(&udp_server::handle_receive, this, boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred)
		);
	}
	void handle_receive(const boost::system::error_code &error, size_t bytes_transferred) {
		start_receive();
		wcout << L"Listening for further commands..." << endl;
		wcout << L"Received size: " << bytes_transferred << endl;
		wcout << L"Received error code: " << error << endl;
		if (bytes_transferred == buffer_size) {
			wcout << L"The command is larger than " << buffer_size-1 << L" characters and can not be interpreted." << endl;
		}
		else {
			wchar_t w_recv_buffer_[bytes_transferred+1];
			recv_buffer_[bytes_transferred] = '\0';
			size_t conversion_result = mbstowcs(w_recv_buffer_, recv_buffer_, bytes_transferred+1);
			if (conversion_result == SIZE_MAX) {
				wcout << L"Invalid multibyte character sequence in command. It cannot be interpreted." << endl;
			}
			else {
				wcout << L"Received command: " << w_recv_buffer_ << endl;
				if (log_file_.is_open()) {
					if (!log_file_) {
						wcout << L"Something went wrong with the log file! Closing it." << endl;
						log_file_.close();
					}
					time_t t = time(nullptr);
					tm *lt = localtime(&t);
					ostringstream oss;
					oss << put_time(lt, "%Y-%m-%d %H:%M:%S,");
					wstring ws = w_recv_buffer_;
					w_replace_all(ws, L"\"", L"\"\"");
					log_file_ << s2ws(oss.str()) << L'"' << ws << L'"' << endl;
				}
				wcout << L"Interpreting received command." << endl;
				interpret(w_recv_buffer_);
			}
		}
	}
public:
	udp_server(boost::asio::io_context &io, wstring log_file = L"")
	// writing received packets to log_file if log_file != L""
		:	socket_(io, udp::endpoint(udp::v4(), server_port))
	{
		if (log_file.size()) {
			wcout << L"Opening log file in append mode..." << endl;
			string log_file_s = ws2s(log_file);
			log_file_.open(log_file_s.data(), ios::out | ios::app);
			if (log_file_.is_open()) {
				wcout << L"Log file opened." << endl;
			}
			else {
				wcout << L"Could not open log file!" << endl;
			}
		}
		start_receive();
		wcout << L"Listening for commands..." << endl;
	}
	~udp_server() {
		if (log_file_.is_open()) {
			log_file_.close();
			wcout << L"Log file closed." << endl;
		}
	}
};

const wstring help =
LR"delim(Usage: HintServer [OPTION]...
Listen on a port for commands to show hints or play media.

Default arguments for options are shown in parenthesis.
  -p, --port             (40000)    port for incoming UDP commands
  -m, --mediadir         (~/Media)  media directory
  -b, --background       (bg.jpg)   background image
  -h, --hint-background  (hbg.jpg)  hint background image
  -l, --log-file         ()         absolute log file path (off by default)
  -t, --hint-timeout     (0)        hint timeout [s] (0 means no timeout)
  -?, --help                        show this help and exit)delim";

void window_event_handler(const boost::system::error_code& e) {
	if (e.value() != boost::asio::error::operation_aborted) {
		xcb_generic_event_t *e;
		while ((e = xcb_poll_for_event(connection)) != nullptr) {
			switch (e->response_type & ~0x80) {
			case XCB_CLIENT_MESSAGE: {
				xcb_client_message_event_t *se = (xcb_client_message_event_t*) e;
				// If the window was closed, exit program.
				if (se->data.data32[0] == wm_delete_window_atom) {
					free (se);
					wcout << L"The window was closed." << endl;
					io.stop();
					return;
				}
				break;
			}
			case XCB_ENTER_NOTIFY: {
				xcb_xfixes_hide_cursor(connection, win);
				xcb_flush(connection);
				break;
			}
			case XCB_LEAVE_NOTIFY: {
				xcb_xfixes_show_cursor(connection, win);
				xcb_flush(connection);
				break;
			}
			default:
				break;
			}
			free(e);
		}
		if (xcb_connection_has_error(connection)) {
			wcout << L"The X connection has an error! Terminating." << endl;
			mp.stop();
			xcb_disconnect(connection);
			exit(EXIT_FAILURE);
		}
		window_timer.expires_after(window_timer_expiry);
		window_timer.async_wait(&window_event_handler);
	}
}

xcb_intern_atom_reply_t* intern_helper(xcb_connection_t *conn, bool only_if_exists, const char *str)
{
	xcb_intern_atom_cookie_t cookie = xcb_intern_atom(conn, only_if_exists, strlen(str), str);
	return xcb_intern_atom_reply(conn, cookie, nullptr);
}

int wmain(int argc, wchar_t *argv[]) {
	try {
		// Set default mediadir.
		char *home = getenv("HOME");
		if (!home) {
			wcout << L"Could not determine home directory!" << endl;
			exit(EXIT_FAILURE);
		}
		size_t homelen = strlen(home);
		wchar_t *whome = new wchar_t[homelen+1];
		mbstowcs(whome, home, homelen+1);
		mediadir = whome;
		delete [] whome;
		size_t whomelen = mediadir.size();
		if ((whomelen > 0) && (mediadir[whomelen-1] != L'/')) {
			mediadir += L'/';
		}
		mediadir += L"Media/";

		string title = "HintServer";
		int screen_num;
		wcout << L"Trying to connect to X Server..." << endl;
		connection = xcb_connect(nullptr, &screen_num);
		if (xcb_connection_has_error(connection)) {
			wcout << L"The X connection has an error! Terminating." << endl;
			xcb_disconnect(connection);
			exit(EXIT_FAILURE);
		}
		wcout << L"Connected to X server successfully." << endl;
		const xcb_setup_t *setup = xcb_get_setup(connection);
		xcb_screen_iterator_t iter = xcb_setup_roots_iterator(setup);
		for (int i = 0; i < screen_num; i++) {
			xcb_screen_next(&iter);
		}
		screen = iter.data;
		win = xcb_generate_id(connection);
		uint32_t winmask = XCB_CW_BACK_PIXEL | XCB_CW_EVENT_MASK;
		uint32_t winval[] = {screen->black_pixel, XCB_EVENT_MASK_ENTER_WINDOW | XCB_EVENT_MASK_LEAVE_WINDOW};
		xcb_create_window(
			connection,
			XCB_COPY_FROM_PARENT,
			win,
			screen->root,
			10,
			10,
			480,
			360,
			20,
			XCB_WINDOW_CLASS_INPUT_OUTPUT,
			screen->root_visual,
			winmask,
			winval
		);

		// Set window title.
		xcb_change_property(connection, XCB_PROP_MODE_REPLACE, win, XCB_ATOM_WM_NAME, XCB_ATOM_STRING, 8, title.size(), title.data());

		// Set delete window event.
		xcb_intern_atom_reply_t *wm_protocols_reply = intern_helper(connection, true, "WM_PROTOCOLS");
		xcb_atom_t wm_protocols_atom = wm_protocols_reply->atom;
		free(wm_protocols_reply);
		if (wm_protocols_atom == XCB_ATOM_NONE) {
			wcout << L"The atom \"WM_PROTOCOLS\" does not exist on the X server!" << endl;
			exit(EXIT_FAILURE);
		}
		xcb_intern_atom_reply_t *wm_delete_reply = intern_helper(connection, false, "WM_DELETE_WINDOW");
		wm_delete_window_atom = wm_delete_reply->atom;
		free(wm_delete_reply);
		if (wm_delete_window_atom == XCB_ATOM_NONE) {
			wcout << L"The atom \"WM_DELETE_WINDOW\" does not exist on the X server!" << endl;
			exit(EXIT_FAILURE);
		}
		xcb_change_property(connection, XCB_PROP_MODE_REPLACE, win, wm_protocols_atom, XCB_ATOM_ATOM, 32, 1, &wm_delete_window_atom);

		// Set fullscreen.
		xcb_intern_atom_reply_t *wm_state_reply = intern_helper(connection, false, "_NET_WM_STATE");
		wm_state_atom = wm_state_reply->atom;
		free(wm_state_reply);
		xcb_intern_atom_reply_t *wm_fullscreen_reply = intern_helper(connection, false, "_NET_WM_STATE_FULLSCREEN");
		wm_fullscreen_atom = wm_fullscreen_reply->atom;
		free(wm_fullscreen_reply);
		xcb_change_property(connection, XCB_PROP_MODE_REPLACE, win, wm_state_atom, XCB_ATOM_ATOM, 32, 1, &wm_fullscreen_atom);

		// Show window.
		xcb_map_window(connection, win);

		// Query Xfixes version to be able to show and hide cursor.
		xcb_xfixes_query_version(connection, 4, 0);

		// Flush outgoing stream.
		xcb_flush(connection);

		if (xcb_connection_has_error(connection)) {
			wcout << L"The X connection has an error! Terminating." << endl;
			xcb_disconnect(connection);
			exit(EXIT_FAILURE);
		}

		// Call window event handler function periodically.
		boost::asio::steady_timer window_timer(io, window_timer_expiry);
		window_timer.expires_after(window_timer_expiry);
		window_timer.async_wait(&window_event_handler);

		mp.setXwindow(win);
		mp.setFullscreen(true);
		InitializeMagick(nullptr);

		for (wchar_t **arg = argv+1; *arg != nullptr; arg++) {
			if (!(wcscmp(*arg, L"--help") && wcscmp(*arg, L"-?"))) {
				wcout << help << endl;
				exit(EXIT_SUCCESS);
			}
			else if (!(wcscmp(*arg, L"--port") && wcscmp(*arg, L"-p"))) {
				if (++arg == nullptr) {
					wcout << L"Port value expected!" << endl;
					exit(EXIT_FAILURE);
				}
				int s_port;
				int n = swscanf(*arg, L"%i", &s_port);
				if (n < 1 || s_port < 0) {
					wcout << L"Specified port is not a valid unsigned integer!" << endl;
					exit(EXIT_FAILURE);
				}
				port = s_port;
			}
			else if (!(wcscmp(*arg, L"--mediadir") && wcscmp(*arg, L"-m"))) {
				if (++arg == nullptr) {
					wcout << L"Media directory expected!" << endl;
					exit(EXIT_FAILURE);
				}
				mediadir = *arg;
				size_t s = mediadir.size();
				if (s > 0 && mediadir[s-1] != L'/') {
					mediadir += L'/';
				}
			}
			else if (!(wcscmp(*arg, L"--background") && wcscmp(*arg, L"-b"))) {
				if (++arg == nullptr) {
					wcout << L"Background image path expected!" << endl;
					exit(EXIT_FAILURE);
				}
				reset_background = background = *arg;
			}
			else if (!(wcscmp(*arg, L"--hint-background") && wcscmp(*arg, L"-h"))) {
				if (++arg == nullptr) {
					wcout << L"Hint background image path expected!" << endl;
					exit(EXIT_FAILURE);
				}
				reset_hint_background = hint_background = *arg;
			}
			else if (!(wcscmp(*arg, L"--log-file") && wcscmp(*arg, L"-l"))) {
				if (++arg == nullptr) {
					wcout << L"Absolute log file path expected!" << endl;
					exit(EXIT_FAILURE);
				}
				log_file = *arg;
			}
			else if (!(wcscmp(*arg, L"--hint-timeout") && wcscmp(*arg, L"-t"))) {
				if (++arg == nullptr) {
					wcout << L"Timeout value expected!" << endl;
					exit(EXIT_FAILURE);
				}
				int r = swscanf(*arg, L"%d", &hint_timeout);
				if (!r) {
					wcout << L"Timeout value is not an integer!" << endl;
					exit(EXIT_FAILURE);
				}
				if (hint_timeout < 0) {
					wcout << L"Timeout cannot be negative!" << endl;
					exit(EXIT_FAILURE);
				}
			}
			else {
				wcout << L"Unrecognized option " << quoted(*arg) << L'!' << endl;
				exit(EXIT_FAILURE);
			}
		}

		wchar_t arg0[] = L"showbg";
		wchar_t *argv[] = {arg0, nullptr};
		cmd_showbg(1, argv);

		udp_server server(io, log_file);
		io.run();

		mp.stop();
		xcb_disconnect(connection);
		wcout << "Bye!" << endl;
	}
	catch (exception &e) {
		wcerr << L"Terminating due to exception: " << e.what() << endl;
		exit(EXIT_FAILURE);
	}
	catch (...) {
		wcerr << L"Terminating due to exception of undetermined type." << endl;
		exit(EXIT_FAILURE);
	}
	return EXIT_SUCCESS;
}

void hint_timeout_handler(const boost::system::error_code& e)
{
	if (e.value() != boost::asio::error::operation_aborted) {
		wstring mrl = s2ws(mp.media()->mrl());
		wstring tmp0 = wstring(L"file://") + mediadir + L"tmp0.png";
		wstring tmp1 = wstring(L"file://") + mediadir + L"tmp1.png";
		if (mrl == tmp0 || mrl == tmp1) {
			wcout << L"Hint Timeout: Switching To Background" << endl;
			wchar_t arg0[] = L"showbg";
			wchar_t *argv[2] = {arg0, nullptr};
			cmd_showbg(1, argv);
		}
	}
}

int cmd_showhint(int argc, wchar_t *argv[]) {
	if (argc < 2) {
		wcout << L"Usage: showhint HINT!" << endl;
		return 1;
	}
	try {
		size_t hlen = wcslen(argv[1]);
		char *c_hint = new char[4*hlen+1];
		wcstombs(c_hint, argv[1], 4*hlen+1);
		string hint = c_hint;
		delete [] c_hint;

		wstring w_hbg = mediadir + hint_background;
		size_t hbg_len = wcslen(w_hbg.data());
		char *c_hbg = new char[4*hbg_len+1];
		wcstombs(c_hbg, w_hbg.data(), 4*hbg_len+1);
		string hbg = c_hbg;
		delete [] c_hbg;

		wostringstream w_out;
		w_out << mediadir << L"tmp" << tmp_index << L".png";
		size_t outlen = w_out.str().size();
		char *c_out = new char[4*outlen+1];
		wcstombs(c_out, w_out.str().data(), 4*outlen+1);
		string out = c_out;
		delete [] c_out;

		tmp_index = !tmp_index;

		const Color black = Color(0, 0, 0, OpaqueOpacity);
		const Color white = Color(QuantumRange, QuantumRange, QuantumRange, OpaqueOpacity);
		const Color red = Color(MaxRGB, 0, 0, OpaqueOpacity);
		const Color transparent = Color(MaxRGB, MaxRGB, MaxRGB, TransparentOpacity);

		Image image;
		image.read(hbg);

		Geometry geo = image.size();

		const double interline_space = 60;
		const double font_size = 80;

		Image caption_outline(geo, transparent);
		caption_outline.backgroundColor(transparent);
		caption_outline.textGravity(CenterGravity);
		caption_outline.textInterlineSpacing(interline_space);
		caption_outline.fontPointsize( font_size );
		caption_outline.fillColor( black );
		caption_outline.strokeWidth( 1000.0 );
		caption_outline.strokeColor( black );
		caption_outline.read("CAPTION:" + hint);

		Image caption_fill(geo, transparent);
		caption_fill.textInterlineSpacing(interline_space);
		caption_fill.backgroundColor(transparent);
		caption_fill.textGravity(CenterGravity);
		caption_fill.fontPointsize( font_size );
		caption_fill.fillColor( white );
		caption_fill.strokeColor( white );
		caption_fill.strokeWidth( 0 );
		caption_fill.read("CAPTION:" + hint);

		image.composite(caption_outline, 0, 0, OverCompositeOp);
		image.composite(caption_fill, 0, 0, OverCompositeOp);

		image.write(out);

		VLC::Media media = VLC::Media(instance, out.data(), VLC::Media::FromPath);
		mp.pause();
		mp.setMedia(media);
		mp.play();

		if (hint_timeout > 0) {
			hint_timer.expires_after(boost::asio::chrono::seconds(hint_timeout));
			hint_timer.async_wait(&hint_timeout_handler);
		}
	}
	catch( Exception &error_ )
	{
	  cout << "Caught ImageMagick exception: " << error_.what() << endl;
	  return 1;
	}
	return 0;
}

int cmd_showbg(int argc, wchar_t *argv[]) {
	wstring bg = mediadir + background;
	size_t bglen = bg.size();
	char *mbs = new char [4*bglen+1];
	wcstombs(mbs, bg.data(), 4*bglen+1);
	VLC::Media media = VLC::Media(instance, mbs, VLC::Media::FromPath);
	mp.pause();
	mp.setMedia(media);
	mp.play();
	delete [] mbs;
	return 0;
}

int cmd_playmedia(int argc, wchar_t *argv[]) {
	if (argc < 2) {
		wcout << L"Usage: playmedia MEDIA!" << endl;
		return 1;
	}
	wstring m = mediadir + argv[1];
	size_t mlen = m.size();
	char *mbs = new char [4*mlen+1];
	wcstombs(mbs, m.data(), 4*mlen+1);
	VLC::Media media = VLC::Media(instance, mbs, VLC::Media::FromPath);
	mp.pause();
	mp.setMedia(media);
	mp.play();
	delete [] mbs;
	return 0;
}

int cmd_exit(int argc, wchar_t *argv[]) {
	io.stop();
	return 0;
}
