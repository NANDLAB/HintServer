# HintServer
## Dependencies
### Libraries
- boost
- libvlc
- vlcpp
- Magick++
- XCB
- XFixes

In Debian or its derivatives enter:

`sudo apt install libboost-all-dev libvlc-bin libvlc-dev libmagick++-dev libxcb1-dev libxcb-xfixes0-dev`

Unfortunately, there is no vlcpp package in the repository, so we have to install it from source.

Enter a directory where you would like libvlcpp to be cloned to.

`git clone https://code.videolan.org/videolan/libvlcpp.git && cd libvlcpp && ./bootstrap && ./configure && sudo make install`

### Personal Libraries
These files should be copied/symlinked to the project directory for compilation.
- misc.h
- wmain.hpp
- tokbase.h
- wtok.h
- wtok.c
- laupbase.h
- laupbase.c
- wlaup.h
- wlaup.c

## Compilation
`make` to compile, `make clean` to remove all compiled files.
